# AMG8833 Infrared sensor 8x8 pixels

AMG8833 Infrared sensor tested on Arduino Uno.

### SCREEN WIRING

GND - GND,
VCC - 5V,
SCL - 13,
SDA - 11,
RES - 9,
DC -  8,
CS -  10,
BL -  3.3v (back light)

### SENSOR WIRING

GND - GND,
VCC - 3.3V,
SCL - SCL,
SDA - SDA
